import React from 'react';
import BackgroundVideo from './components/backgroundVideo/BackgroundVideo';
import ReactRotatingText from 'react-rotating-text';
import MapSection from './components/map';
import FormSection from './components/form/Form';
import Footer from './components/footer/Footer';
import FadeIn from 'react-fade-in';
import './App.css';

function App() {
  const location = {
    address: '171 Township Rd 185 SW, Junction City, OH 43748',
    lat: 39.721862,
    lng: -82.3091,
  }
  return (
    <div className="App">
      <FadeIn>
        <BackgroundVideo />
        <div className="home-container">
          <h2 id="whatwedo" className="about-header">Welcome To Northern Contracting</h2>
          <p className="about-para">We have been in the industry for years, and have an understanding of the work
          that needs to be done at a price that is easily attainable. For whatever project
          you need. You have us here at Northern to help.</p>
          <div className="rotating-container">
            <ReactRotatingText className="rotating-text" color="#A28841" cursor={false} typingInterval={0} eraseMode="overwrite" items={['Excavating','Grading','Clearing']} />
            <h2 style={{fontSize: '3rem', textAlign: 'center', textTransform: 'uppercase', fontWeight: 'bold', color: 'white'}}>Services</h2>
            <ReactRotatingText className="rotating-text" color="#A28841" cursor={false} typingInterval={0} eraseMode="overwrite" items={['Demolition','Driveways']} />
          </div>
          <div className="tri-container">
            <div className="tri-box">
              <h3 className="tri-box-icon">.01</h3>
              <p>Our company has over thirty five years of combined experience! We have the experience and knowledge to get all tasks done in a professional manner!</p>
            </div>
            <div className="tri-box-seperator" />
            <div className="tri-box">
              <h3 className="tri-box-icon">.02</h3>
              <p>All of our operators are certified, and extremely skilled in there craft. Operating in confined spaces, or rough terrain is no problem! We can do this while following all modern day safety standards.</p>
            </div>
            <div className="tri-box-seperator" />
            <div className="tri-box">
              <h3 className="tri-box-icon">.03</h3>
              <p>We use top of the line equipment allowing for the most precise measurements, and the correct grade to be checked by our GPS run machines. This insures you are getting highest quality you deserve!</p>
            </div>
          </div>
        </div>
        <div className="about-container">
          <div className="about-box">
            <h2 className="about-h2">About Us</h2>
            <div className="div-seperator"/>
            <p>We are a small family owned company dedicated to giving our customers the best experience possible. Our company is built on three pillars; Honesty, Trust, and Professionalism. We are always honest with our customers in giving them the best deal possible, and not making you pay more then you should for your desired job. We hope all of our clients can put 100% of there trust in us while working for us. We strive to be professional 100% of the time. Ensuring our customers have the best Northern Contracting experience possible!</p>
          </div>
          <div className="about-box">
            <video autoPlay="autoplay" loop="loop" muted className="video" >
              <source src="./video/aerial.mp4" type="video/mp4" />
              Your browser does not support the video tag.
            </video>
          </div>
        </div>
        <MapSection location={location} zoomLevel={17} />
        <FormSection />
        <Footer />
      </FadeIn>
    </div>
  );
}

export default App;