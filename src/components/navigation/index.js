import React, { useState } from 'react';
import { GiHamburgerMenu } from 'react-icons/gi';
import { Drawer } from '@material-ui/core';
import './index.css';

const Nav = () => {
    const [open, isOpen] = useState(false);

    return (
        <div className="nav-container">
            <img className="company-logo" src="/images/LOGO.svg" alt="Company Logo" />
            <div className="nav-item-container">
                <a style={{color: '#FED669'}} className="nav-item uppercase" href="">Home</a>
                <a className="nav-item uppercase" href="#whatwedo">What We Do</a>
                <a className="nav-item uppercase" href="#location">Location</a>
                <a className="nav-item uppercase" href="#contact">Contact</a>
            </div>
            <GiHamburgerMenu onClick={()=> isOpen(true)} className="hamburger-icon"/>
            <Drawer
                ModalProps={{ onClose : ()=>isOpen(false)}}
                docked='false'
                anchor='right'
                open={open}>
                <div className="drawer-container">
                    <img className="drawer-logo" src="./images/LOGO.svg" alt="" />
                    <div>
                        <a onClick={()=> isOpen(false)} className="nav-btn" href="#home">Home</a>
                    </div>
                    <div>
                        <a onClick={()=> isOpen(false)} className="nav-btn" href="#whatwedo">WHAT WE DO</a>
                    </div>
                    <div>
                        <a onClick={()=> isOpen(false)} className="nav-btn" href="#location">Location</a>
                    </div>
                    <div>
                        <a onClick={()=> isOpen(false)} className="nav-btn" href="#contact">CONTACT</a>
                    </div>
                    <button onClick={()=> isOpen(false)} className="btn-close">Close</button>
                </div>
            </Drawer>
        </div>
    )
}
export default Nav;