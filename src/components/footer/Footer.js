import React from 'react';
import './Footer.css';

const Footer = () => (
    <div className="container-footer">
        <p className="copy-text">&copy;2020 by Northern Contracting Co.</p>
        <p className="copy-text">ALL RIGHTS RESERVED.</p>
        <div className="container-lovast">
            <img className="lovast-logo" src="./images/lovast_logo.svg" alt="" />
            <p className="lovast-text">Created by Lovast</p>
        </div>
    </div>
)
export default Footer;