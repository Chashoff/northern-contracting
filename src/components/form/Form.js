import React, { useState } from 'react';
import axios from 'axios';
import Reaptcha from 'reaptcha';
import './Form.css';

function Form(){
    const [btnText, setBtn] = useState('Submit');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [message, setMessage] = useState('');
    const [recaptchaResponse, setResponse] = useState('');
    const [isValid, setValid] = useState(false);
    
    const onVerify = res => {
        setResponse(res);
        setValid(true);
      }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const body = {
            sender: 'northern',
            gResponse: recaptchaResponse,
            name: name,
            email: email,
            phone: phone,
            message: message,
            emails: ['management@lovast.com']
        };
        const res = await fetch('https://lovast.com/api/v1/comms/email', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(body),
        });
        if (res.status === 201) {
            isValid(false);
            setBtn('Form submitted!')
        } else {
            isValid(false);
            btnText('Error, reload page!');
        }
    };

    return (
        <div id="contact" className="form-container">
            <div className="form-box">
                <h2 className="contact-h2">CALL US FOR A FREE QUOTE <span style={{color: '#FED669'}}>(740)-605-2930</span></h2>
            </div>
            <div className="form-box">
                <h3 className="contact-h3">Lets get in touch!</h3>
                <form className="contact-form" name="contact" method="POST" onSubmit={handleSubmit}>
                    <input className="contact-input" type="text" id="name" name="name" placeholder="Name" value={name} onChange={e => setName(e.target.value)} />
                    <input className="contact-input" type="email" id="email" name="email" placeholder="Email" value={email} onChange={e => setEmail(e.target.value)} />
                    <input className="contact-input" type="phone" id="phone" name="phone" placeholder="Phone Number" value={phone} onChange={e => setPhone(e.target.value)} />
                    <textarea rows="6" className="contact-input" type="message" id="message" name="message" placeholder="Your message..." value={message} onChange={e => setMessage(e.target.value)} />
                    <Reaptcha sitekey="6Leil6gUAAAAAFjtE3clAglFBnIraxn1O9mzDFCT" onVerify={onVerify}/>
                    <button className="contact-submit-btn" type="submit">{btnText}</button>
                </form>
            </div>
        </div>
    )
}
export default Form;